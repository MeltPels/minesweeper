# Minesweeper

Minesweeper is my first full-fledged project with a JSwing GUI. It keeps track of your score and is fully playable.
Latest update: 30-05-2023

## Getting started

Run the program with your favorite IDE. I've used NetBeans to program this. 

## About the program

This is my first project where I used what I knew about Java to program Minesweeper. 
I'm using a 2D Array with a user-definable amount of bombs and field-size to populate the field. When you click, it checks if the coordinates of the button you clicked, is a bomb or a safe space. If it's safe, it calculates the amount of bombs around it and shows it on the button. If its zero, it will do the same for its direct neighbours, to get the cascading effect.


## Authors and acknowledgment
This project was coded by Melt Pels. I've received a lot of explanation and help from people inside QQuest, the company that's training me to learn basic Java development skills. 


## License
You are free to use and edit parts or the entirety of this code. Make sure you don't copy and actually understand what everything does before you implement the code yourself. It's a great way to learn!

## Project status
This project is up for review. Feedback is welcome, this is a learning excersize.
