/*  
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mijnenveger_3;

/**
 *
 * @author Melt Pels
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;

public class ButtonHandler extends MouseAdapter implements ActionListener {
    public static int x;
    public static int y;
    JButton button;
    public static boolean rightButton;

    public void actionPerformed(ActionEvent e) {
        button = (JButton) e.getSource();
        x = (int) button.getClientProperty("column");
        y = (int) button.getClientProperty("row");
        synchronized (Mijnenveger_3.lock) {
            Mijnenveger_3.lock.notify();
        }
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON3) {
            rightButton = true;
            button = (JButton) e.getSource();
            x = (int) button.getClientProperty("column");
            y = (int) button.getClientProperty("row");
            synchronized (Mijnenveger_3.lock) {
                Mijnenveger_3.lock.notify();
            }
        }
    }

    public int getColumn() {
        return x;
    }

    public int getRow() {
        return y;
    }
}