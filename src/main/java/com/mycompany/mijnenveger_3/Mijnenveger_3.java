/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.mijnenveger_3;

/**
 *
 * @author Melt Pels
 */
public class Mijnenveger_3 {
    final static int height = 10;
    final static int width = 20;
    final static int totalBombs = 20;
    public static boolean gameOver = false;
    static Mijnenveld minefield = new Mijnenveld(width, height);

    public static final Object lock = new Object();
    static MijnenvegerGUI gui = new MijnenvegerGUI(width,height);

    public static void main(String[] args) {

        minefield.addRandomBombs(totalBombs);
        startGame();

    }

    public static void startGame() {
        while (!gameOver) {
            waitForInput();
            testInput();
            isWinconditionMet();
        }
    }

    public static void waitForInput() {
        synchronized (lock) {
            try {
                lock.wait(); // Wait for notification from ActionHandler
            } catch (InterruptedException e) {
            }
        }
    }

    public static void testInput() {
        if (ButtonHandler.rightButton) { // Was this a right-click?
            markFlag(ButtonHandler.x, ButtonHandler.y);
        } else if (minefield.isBomb(ButtonHandler.x, ButtonHandler.y)) { // left click
            gui.scoreboard.setText("GAME OVER! Score: " + Mijnenveger_3.minefield.winCondition);
            revealBombs();
            gameOver = true;
        } else if (minefield.numNeighbourBombs(ButtonHandler.x, ButtonHandler.y) > 0) {
            revealBombNeighbours();
            minefield.markChecked(ButtonHandler.x, ButtonHandler.y);
        } else if (minefield.numNeighbourBombs(ButtonHandler.x, ButtonHandler.y) == 0) {
            revealBombNeighbours();
            checkSafeNeighbours(ButtonHandler.x, ButtonHandler.y);
        }
    }

    public static void revealBombs() {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (gameOver) {
                    if (minefield.isBomb(x, y)) {
                    gui.setBomb(x, y);
                    gui.setColorGreen(x, y);
                    }
                } else {
                    if (minefield.isBomb(x, y)) {
                        gui.setBomb(x, y);
                        gui.setColorRed(x, y);
                        }
                }
            }
        }
    }

    public static void revealBombNeighbours() {
        int neighbourBombs = minefield.numNeighbourBombs(ButtonHandler.x, ButtonHandler.y);
        String s = Integer.toString(neighbourBombs);
        gui.setText(ButtonHandler.x, ButtonHandler.y, s);
        gui.setColorGray(ButtonHandler.x, ButtonHandler.y);
    }

    public static void revealSafeNeighbours(int x, int y) {
        if (minefield.minefield.withinLimit(x, y) && (minefield.isBomb(x, y) == false && (minefield.isChecked(x, y) == false))) {
            int neighbourBombs = minefield.numNeighbourBombs(x, y);
            String s = Integer.toString(neighbourBombs);
            gui.setText(x, y, s);
            gui.setColorGray(x, y);
        }
    }

    public static void checkSafeNeighbours(int x, int y) {
        if (minefield.isChecked(x, y)) {
            return;
        }
        expandDiscoveredZeroes(x, y);
    }

    private static void expandDiscoveredZeroes(int x, int y){
        minefield.markChecked(x, y);
        revealSafeNeighbours(x - 1, y);
        revealSafeNeighbours(x + 1, y);
        revealSafeNeighbours(x, y + 1);
        revealSafeNeighbours(x, y - 1);
        continueSearch(x-1,y); // left
        continueSearch(x+1,y); // right
        continueSearch(x,y-1); // down
        continueSearch(x,y+1); // up
    }

    private static void continueSearch(int x, int y){
        if (!minefield.isChecked(x, y) && minefield.numNeighbourBombs(x, y) == 0) {
            checkSafeNeighbours(x, y);
        } else {
            minefield.markChecked(x + 1, y);
        }
    }

    private static void isWinconditionMet() {
        if (minefield.winCondition == width * height - totalBombs) {
            gui.scoreboard.setText("VICTORY! Score: " + minefield.winCondition);
            gameOver = true;
            revealBombs();
        }
        System.out.println(minefield.winCondition + " out of " + (width * height - totalBombs) + " fields checked.");
    }

    private static void markFlag(int x, int y) {
        gui.setBomb(x, y);
        ButtonHandler.rightButton = false;
    }
}
