/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mijnenveger_3;

import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Color;

/**
 *
 * @author Melt Pels
 */
public class MijnenvegerGUI extends JFrame {
    JFrame gui = new JFrame();
    JPanel buttons = new JPanel();
    JPanel container = new JPanel();
    JPanel header = new JPanel();
    JLabel scoreboard = new JLabel("Score: " + Mijnenveger_3.minefield.winCondition);
    JButton[][] knopArray;
    ImageIcon bombIcon = new ImageIcon(MijnenvegerGUI.class.getResource("/resources/bomb.png"));
    
    

    public MijnenvegerGUI(int width, int height) {
        knopArray = new JButton[width][height];
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        container.setLayout(new BorderLayout());
        container.add(header, BorderLayout.NORTH);
        buttons.setLayout(new GridLayout(height, width));
        buttons.setPreferredSize(new Dimension((width * 45), (height * 45 + 20)));
        container.add(buttons, BorderLayout.CENTER);
        header.add(scoreboard);

        for (int x = 0; x < height; x++) {
            for (int y = 0; y < width; y++) {
                buttons.add(knopArray[y][x] = new JButton(" "));
                knopArray[y][x].addActionListener(new ButtonHandler());
                knopArray[y][x].addMouseListener(new ButtonHandler());
                knopArray[y][x].putClientProperty("row", x);
                knopArray[y][x].putClientProperty("column", y);

            }
        }

        gui.getContentPane().add(container);
        gui.pack();
        gui.setVisible(true);
    }

    public void setText(int x, int y, String s) {
        knopArray[x][y].setText(s);
        knopArray[x][y].setIcon(null);
        scoreboard.setText("Score: " + Mijnenveger_3.minefield.winCondition);
    }

    public void setColorGray(int x, int y) {
        knopArray[x][y].setBackground(Color.LIGHT_GRAY);
    }

    public void setColorGreen(int x, int y) {
        knopArray[x][y].setBackground(Color.GREEN);
    }

    public void setColorRed(int x, int y) {
        knopArray[x][y].setBackground(Color.RED);
    }

    public void setBomb(int x, int y) {
        knopArray[x][y].setIcon(bombIcon);
        knopArray[x][y].setHorizontalAlignment(SwingConstants.CENTER);
        knopArray[x][y].setVerticalAlignment(SwingConstants.CENTER);
    }

}
