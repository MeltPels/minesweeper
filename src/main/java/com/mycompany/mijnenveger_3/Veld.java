/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mijnenveger_3;

/**
 *
 * @author Melt Pels
 */
public class Veld {

    private final int width;
    private final int height;
    private final int[][] grid;

    public Veld(int width, int height) {
        this.width = width;
        this.height = height;
        this.grid = new int[height][width];
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getValue(int x, int y) {
        if (withinLimit(x, y)) {
            return grid[y][x];
        }
        throw new IllegalArgumentException("The requested value is outside of the array's boundaries." + "\nArray is " + grid.length + " wide and " + grid[0].length + "long. Received value: " + x + "," + y);
    }

    public void setWaarde(int x, int y, int value) {
        if (withinLimit(x, y)) {

            grid[y][x] = value;
        }
    }

    public boolean withinLimit(int x, int y) {
        return (x >= 0 && x < grid[0].length && y >= 0 && y < grid.length);
    }

}
