/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mijnenveger_3;

/**
 *
 * @author Melt Pels
 */
public class Mijnenveld {
    public Veld minefield;
    private final int width;
    private final int height;
    private boolean[][] validation;
    public int winCondition = 0;

    public Mijnenveld(int width, int height) {
        this.width = width;
        this.height = height;
        minefield = new Veld(width, height);
        validation = new boolean[width][height];
    }

    public void addRandomBombs(int numBombs) {
        for (int i = 0; i < numBombs; i++) {
            int newX = (int) Math.floor(Math.random() * width);
            int newY = (int) Math.floor(Math.random() * height);
            if (!isBomb(newX, newY)) {
                addBomb(newX, newY);
            }
        }
    }

    public void addBomb(int x, int y) {
        minefield.setWaarde(x, y, 10);
    }

    public boolean isBomb(int x, int y) {
        return (minefield.getValue(x, y) == 10);
    }

    public void markChecked(int x, int y) {
        if (minefield.withinLimit(x, y) && validation[x][y] == false) {
            validation[x][y] = true;
            winCondition++;
        }

    }

    public boolean isChecked(int x, int y) {
        if (minefield.withinLimit(x, y)) {
            return validation[x][y];
        }
        return true;
    }

    public int numNeighbourBombs(int x, int y) {
        int bombCounter = 0;
        if (minefield.withinLimit(x - 1, y) && isBomb(x - 1, y)) {
            bombCounter++;
        }
        if (minefield.withinLimit(x - 1, y - 1) && isBomb(x - 1, y - 1)) {
            bombCounter++;
        }
        if (minefield.withinLimit(x, y - 1) && isBomb(x, y - 1)) {
            bombCounter++;
        }
        if (minefield.withinLimit(x + 1, y - 1) && isBomb(x + 1, y - 1)) {
            bombCounter++;
        }
        if (minefield.withinLimit(x + 1, y) && isBomb(x + 1, y)) {
            bombCounter++;
        }
        if (minefield.withinLimit(x + 1, y + 1) && isBomb(x + 1, y + 1)) {
            bombCounter++;
        }
        if (minefield.withinLimit(x, y + 1) && isBomb(x, y + 1)) {
            bombCounter++;
        }
        if (minefield.withinLimit(x - 1, y + 1) && isBomb(x - 1, y + 1)) {
            bombCounter++;
        }
        return bombCounter;
    }

    /**
     * Call printField(); to print a cheat-sheet to the console. In the cheat-sheet, the 10's are bombs, the 00's are safe.
     */
    public void printField() {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                String padded = String.format("%02d", minefield.getValue(x, y));
                System.out.print("[ " + padded + " ]");
            }
            System.out.println();
        }
    }
}
